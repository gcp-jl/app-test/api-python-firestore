FROM python:3.8.2-alpine3.11

LABEL maintainer "jey.seven@gmail.com"	

ENV PORT 8080

EXPOSE 8080

COPY requirements.txt ./ 

RUN apk add --no-cache --virtual .build-deps gcc musl-dev g++ linux-headers

RUN pip install --no-cache-dir --upgrade -r requirements.txt  

RUN apk del .build-deps

COPY aplicacion.py ./

CMD [ "python3", "./aplicacion.py"]
