from flask import request, jsonify, Flask
from flask_restful import Resource, Api
from google.cloud import firestore
import os

puerto = os.getenv('PORT')
listen = '0.0.0.0'

db = firestore.Client()

app = Flask(__name__)
api = Api(app)


class listUsuarios(Resource):
    def get(self):  
        read = db.collection(u'usuario').stream() 
        lista = []
        result = {}
        for doc in read:
            lista.append(doc.id)
        result['Usuarios'] = lista
        return jsonify(result)


class Usuarios(Resource):
    def get(self, user):
        readDoc = db.collection(u'usuario').stream()
        result = {}
        for doc in readDoc:
            if doc.id in user:
                result[doc.id] = doc.to_dict()
        return jsonify(result)

    def post(self,user): 
        create = db.collection(u'usuario').document(user)
        last_name = request.json['LastName']
        first_name = request.json['FirstName']
        birth_date = request.json['BirthDate']
        city = request.json['City']
        state = request.json['State']
        country = request.json['Country']
        write = create.set(request.json)
        return {'status': 'New user add'}
    
class delUsuario(Resource):
    def delete(self,user):
        delete = db.collection(u'usuario').document(user).delete()
        return {'status': 'User delete'}

api.add_resource(listUsuarios, '/usuarios/all')  # Route_1
api.add_resource(Usuarios, '/usuarios/<user>')  # Route_2
api.add_resource(delUsuario, '/delete/<user>')  # Route_3

if __name__ == '__main__':
     #from waitress import serve        
     #serve(app, host="0.0.0.0", port=8080)
     app.run(port=puerto,host=listen)

